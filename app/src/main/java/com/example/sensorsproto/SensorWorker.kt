package com.example.sensorsproto

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.SystemClock.sleep
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters


class SensorWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {


    override fun doWork(): Result {
        var sensorManager: SensorManager = applicationContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        var accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)


        class AccelerometerListener : SensorEventListener {
            override fun onSensorChanged(event: SensorEvent) {
                // ici on a la valeur du capteur toutes les 20 millisecondes

                // à accumuler, et à envoyer au serveur toutes les 6 secondes, cad quand on a 300 mesures (à faire)

                Log.v("Worker", event.values[0].toString())

                // pour arrêter le worker
                if (isStopped) sensorManager.unregisterListener(this)
            }

            override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
        }

        val listener = AccelerometerListener()
        sensorManager.registerListener(
            listener,
            accel,
            20000 /* 20000 micro secondes, soit 20 millisecondes */
        );

        // on attend à l'infini
        while (true) sleep(10000)

        // Indicate whether the work finished successfully with the Result
        return Result.success()
    }
}
