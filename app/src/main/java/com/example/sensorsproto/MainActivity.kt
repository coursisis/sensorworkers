package com.example.sensorsproto

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkRequest
import com.example.sensorsproto.ui.theme.SensorsProtoTheme

class MainActivity : ComponentActivity() {

    private val readSensorRequest: WorkRequest =
        OneTimeWorkRequestBuilder<SensorWorker>()
            .build()

    private fun startWork() {
        WorkManager
            .getInstance(baseContext)
            .enqueue(readSensorRequest)

    }

    private fun stopWork() {
        WorkManager.getInstance(baseContext).cancelWorkById(readSensorRequest.id)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        setContent {
            SensorsProtoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column(verticalArrangement = Arrangement.SpaceEvenly) {
                        Text("Workmanager et sensors")
                        Button(onClick = { startWork() }) {
                            Text("Start")
                        }
                        Button(onClick = { stopWork()}) {
                            Text("Stop")
                        }

                    }

                }
            }
        }
    }
}